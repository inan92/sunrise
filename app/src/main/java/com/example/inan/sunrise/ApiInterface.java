package com.example.inan.sunrise;

import com.example.inan.sunrise.POJOS.CurrentWeather;
import com.example.inan.sunrise.POJOS2.Forecast;
import com.example.inan.sunrise.POJOS2.List;
import com.example.inan.sunrise.POJOS2.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Inan on 10/7/2017.
 */

public interface ApiInterface {

    @GET()
    Call<CurrentWeather> getWeather(@Url String urlString);

    @GET()
    Call<Forecast> getResults(@Url String urlString);
}
