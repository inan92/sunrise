package com.example.inan.sunrise;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.inan.sunrise.POJOS.CurrentWeather;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

public class MainActivity extends AppCompatActivity {


    TextView tempTV, cityTV,weathertypeTV, humidityTV,windTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tempTV = (TextView) findViewById(R.id.current_temp);
        cityTV = (TextView) findViewById(R.id.city_name);
        weathertypeTV = (TextView) findViewById(R.id.weather_type);
        humidityTV = (TextView)findViewById(R.id.humidity);
        windTV = (TextView)findViewById(R.id.wind_speed);

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        String urlString = "weather?q=Dhaka&units=metric&appid=7fffb16e9b4f308fb2a484c29c732892";

        Call<CurrentWeather> call = apiInterface.getWeather(urlString);

        call.enqueue(new Callback<CurrentWeather>() {
            @Override
            public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                if(response.code() == 200){
                    CurrentWeather weather = response.body();
                    tempTV.setText(weather.getMain().getTemp().toString()+"° C");
                    cityTV.setText(weather.getName());
                  //  weathertypeTV.setText((CharSequence) weather.getWeather());
                    humidityTV.setText("Humidity: "+weather.getMain().getHumidity()+"%");
                    windTV.setText("Wind Speed: " +weather.getWind().getSpeed()+" m/s");
                }

            }

            @Override
            public void onFailure(Call<CurrentWeather> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);

        MenuItem item = menu.findItem(R.id.search_action);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {


                ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                String urlString = String.format("weather?q=%s&units=metric&appid=7fffb16e9b4f308fb2a484c29c732892",query);

                Call<CurrentWeather> call = apiInterface.getWeather(urlString);

                call.enqueue(new Callback<CurrentWeather>() {
                    @Override
                    public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                        if(response.code() == 200){
                            CurrentWeather weather = response.body();

                                tempTV.setText(weather.getMain().getTemp().toString()+"° C");
                                cityTV.setText(weather.getName());
                                //weathertypeTV.setText(weather.);
                                humidityTV.setText("Humidity: "+weather.getMain().getHumidity()+"%");
                                windTV.setText("Wind Speed: " +weather.getWind().getSpeed()+" m/s");

                        }

                    }

                    @Override
                    public void onFailure(Call<CurrentWeather> call, Throwable t) {

                    }
                });

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_action:
                break;

            case R.id.action_forecast:
                Intent intent = new Intent(MainActivity.this,ForecastActivity.class);
                String s= cityTV.getText().toString();
                intent.putExtra("City",s);
                startActivity(intent);

        }
        return true;
    }


}
