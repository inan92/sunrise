package com.example.inan.sunrise;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.inan.sunrise.POJOS2.Forecast;
import com.example.inan.sunrise.POJOS2.List;
import com.example.inan.sunrise.POJOS2.Weather;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Inan on 10/8/2017.
 */

public class ForecastAdapter extends ArrayAdapter<List> {

    private java.util.List<List> weatherList;
    private Context context;
   // private int resource;


    public ForecastAdapter(Context context, java.util.List<List> list){
        super(context,R.layout.single_row_item,list);
        this.context = context;
        this.weatherList = list;


    }
    private static class ViewHolder{
        TextView dateView,temp,humidity,windspeed;

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        List list = getItem(position);
        ViewHolder viewHolder = null;

        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.single_row_item,parent,false);
            viewHolder.dateView = (TextView) convertView.findViewById(R.id.date_text_view);
            viewHolder.temp = (TextView) convertView.findViewById(R.id.temp_text_view);
            viewHolder.humidity = (TextView) convertView.findViewById(R.id.humidity_text_view);
            viewHolder.windspeed = (TextView) convertView.findViewById(R.id.wind_speed_text_view);
        }

        viewHolder.temp.setText("Temperature: "+list.getMain().getTemp().toString()+"°C");
        if(position==0){
            viewHolder.dateView.setText("Today");
        }
        else if(position==1){
            viewHolder.dateView.setText("Tomorrow");
        }
        else{
            Date createdOn = new Date(list.getDt()*1000);
            String date = DateFormat.getDateTimeInstance().format(createdOn);
            viewHolder.dateView.setText(date);
        }

        viewHolder.humidity.setText("Humidity: "+list.getMain().getHumidity()+"%");
        viewHolder.windspeed.setText("WindSpeed: "+list.getWind().getSpeed()+"m/s");


        return convertView;
    }
}
