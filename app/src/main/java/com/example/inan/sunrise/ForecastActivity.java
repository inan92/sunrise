package com.example.inan.sunrise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ListView;

import com.example.inan.sunrise.POJOS.CurrentWeather;
import com.example.inan.sunrise.POJOS2.Forecast;
import com.example.inan.sunrise.POJOS2.List;
import com.example.inan.sunrise.POJOS2.Weather;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForecastActivity extends AppCompatActivity {

    ListView forecastList;
    String s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        forecastList = (ListView) findViewById(R.id.forecast_list);


        s = getIntent().getStringExtra("City");

        Log.i("q",s);


        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        String urlString = String.format("forecast?q=%s&appid=b1fd1a550ee0787d21636ee084af59ba",s);

        Call<Forecast> call = apiInterface.getResults(urlString);

        call.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {

                if(response.code() == 200){
                   Forecast weatherList = response.body();

                    java.util.List<List> list = weatherList.getList();

                  //  Log.i("weather",list.get(0).getMain().toString());
                  //  Log.i("weather",weatherList.getWeather().get(0).getMain().toString());
                   forecastList.setAdapter(new ForecastAdapter(getApplicationContext(),list));
                   // Log.i("list",list.toString());
                  //  Log.i("list",list.getMain().toString());


                }
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }
}
